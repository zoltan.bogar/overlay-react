import styles from './spinner.module.css';

const Spinner = () => {
  return (
    <>
      <div className={styles.spinner}></div>
      <div className={styles.spinnerLabel}>Loading</div>
    </>
  );
}

export default Spinner;