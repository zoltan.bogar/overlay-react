import Backdrop from "./backdrop/backdrop";
import Spinner from "./spinner/spinner";

import styles from "./LoadingLayer.module.css";

const LoadingLayer = () => {
  return (
    <div className={styles.loadingLayer}>
      <Backdrop />
      <Spinner />
    </div>
  );
}

export default LoadingLayer;