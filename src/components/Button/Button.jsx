import React from "react";
import styles from './Button.module.css';
import {useOverlayStore} from "../../store/overlay.js";

const Button = ({setKey, arrayLength}) => {
  const toggleOverlayVisibility = useOverlayStore((state) => state.toggleVisibility)

  const handleOverlayTrigger = () => {
    toggleOverlayVisibility();

    setKey(Math.floor(Math.random() * arrayLength));
  }

  return (
    <button
      className={styles.button}
      onClick={handleOverlayTrigger}
    >
      Trigger overlay
    </button>
  );
}

export default Button;