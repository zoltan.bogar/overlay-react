import React from "react";

import {formatDate} from "../../util/dateFormat.js";

import styles from './Overlay.module.css';

const Overlay = ({name, from, to, talent, color}) => {
  return (
    <div className={styles.container}>
      <div
        className={styles.name}
        style={{backgroundColor: color}}
      >
        {name}
      </div>
      <div className={styles.date}>{formatDate(from)} - {formatDate(to)}</div>
      <div
        className={styles.talent}
      >
        {talent.map(individual => {
          return <div
            className={styles['talent__item']}
            key={individual}
            style={{backgroundColor: color}}
          >
            {individual}
          </div>
        })}
      </div>
    </div>
  );
}

export default Overlay;