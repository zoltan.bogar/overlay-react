import React, {useState} from 'react';

import useFetch from "react-fetch-hook";
import {useOverlayStore} from "./store/overlay.js";
import LoadingLayer from "./components/LoadingLayer/LoadingLayer";
import Button from "./components/Button/Button.jsx";
import Overlay from "./components/Overlay/Overlay.jsx";
import styles from './App.module.css';

const arrayLength = 3;

function App() {
  const {isLoading, error, data} = useFetch('./data.json');
  const [key, setKey] = useState(Math.floor(Math.random() * arrayLength));
  const visible = useOverlayStore((state) => state.visible);

  if (isLoading) return <LoadingLayer />;

  return (
    <main className={styles.canvas}>
      {visible && (
        <Overlay
          name={data[key].name}
          from={data[key].from}
          to={data[key].to}
          talent={data[key].talent}
          color={data[key].color}
        />)}
      <Button
        setKey={setKey}
        arrayLength={arrayLength}
      />
    </main>
  )
}

export default App
