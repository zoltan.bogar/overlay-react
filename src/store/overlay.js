import { create } from 'zustand';

export const useOverlayStore = create((set) => ({
  visible: false,
  toggleVisibility: () => set((state) => ({visible: !state.visible})),
}));