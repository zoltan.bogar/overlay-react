# Instructions:
1. Create a Javascript application (vue or vanilla js) that visualises the attached data.
2. Use SCSS for styling.
3. The overlay should be hidden by default.
4. For the sake of simplicity, put a button to the top right corner that toggles the overlay.
5. The coloured parts of the overlay should use the colour from the data.
6. Data should not be hardcoded into the HTML, but read from a json file or something you prefer.
7. Everytime the overlay is toggled on, a new random event should be displayed.
8. Try and make it so the application matches the concept as close as possible (nearly pixel perfect)
# Overlay visuals:
1. Use this image as a reference https://drive.google.com/file/d/1JpspYYUv83QAqEaepJ4g8Q3TYI5tnXD4/view?usp=sharing
2. The canvas must be 1920px wide and 950px high
3. Everything should be centred
4. Every box should support multi-line break
5. Talent name box layout is based on the amount of entries
   - First box has left aligned text on multi-line
   - Last box has right aligned text on multi-line
